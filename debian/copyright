Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Transmission remote GUI
Upstream-Contact: Transmission Remote GUI working group https://github.com/transmission-remote-gui
Source: https://github.com/transmission-remote-gui/transgui
Files-Excluded: setup/win/openssl/*
                setup/win_amd64/openssl/*
Comment: Upstream are violating GPL by distributing OpenSSL binaries

Files: *
Copyright: 2008-2014, Yury Sidorov <jura@cp-lab.com>
           2014-2019, Transmission Remote GUI working group
License: GPL-2

Files: synapse/*
Copyright: 1999-2002, Lukas Gebauer
           2015-2019, Transmission Remote GUI working group
License: custom-license
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of Lukas Gebauer nor the names of its contributors may
 be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

Files: debian/*
Copyright: 2010-2019, Andreas Noteng <andreas@noteng.no>
License: GPL-3+

Files: debian/patches/fix_compile.patch
Copyright: 2017, Yury Sidorov <jura@cp-lab.com>
           2017, Andreas Noteng <andreas@noteng.no>
License: GPL-2

Files: gcontnrs.pas
Copyright: 2014, Yann Mérignac
License: LGPL-2.1+

Files: debian/patches/force_ssl1.0.2.patch
Copyright: 2017, Sebastian Andrzej Siewior <sebastian@breakpoint.cc>
           2017, Andreas Noteng <andreas@noteng.no>
License: GPL-3+

Files: GeoIP.pas
Copyright: 2005, MaxMind LLC
License: GPL-2+

Files: lineinfo2.pp
Copyright: 2000, Peter Vreman
License: LGPL2.1 with linking exception

Files: maclocale.pas
Copyright: 2013, Yury Sidorov
License: LGPL2.1 with linking exception

License: LGPL2.1 with linking exception
 The source code of the Free Pascal Runtime Libraries and packages are
 distributed under the Library GNU General Public License
 (see the file COPYING) with the following modification:
 .
 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent modules,
 and to copy and distribute the resulting executable under terms of your choice,
 provided that you also meet, for each linked independent module, the terms
 and conditions of the license of that module. An independent module is a module
 which is not derived from or based on this library. If you modify this
 library, you may extend this exception to your version of the library, but you are
 not obligated to do so. If you do not wish to do so, delete this exception
 statement from your version.

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of this license can be found in
 /usr/share/common-licenses/GPL-3.

License: LGPL-2.1
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
